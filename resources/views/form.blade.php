<h1>Buat Account Baru!</h1>

<h3>Sign Up Form</h3>



<form action="/welcome" method="POST">
@csrf 
    <label for="nama_depan">First name:</label>
    <br>
    <br>
    <input type="text" id="nama_depan" name="nama_depan">
    <br>
    <br>
    <label for="nama_belakang">Last name:</label>
    <br>
    <br>
    <input type="text" id="nama_belakang" name="nama_belakang">
    <br>
    <br>
    <label for="gender">Gender:</label>
    <br>
    <br>
    <input type="radio" id="male" name="jk" value="male">
    <label for="male">Male</label><br>

    <input type="radio" id="female" name="jk" value="female">
    <label for="female">Female</label><br>

    <input type="radio" id="other" name="jk" value="other">
    <label for="other">Other</label>
    <br>
    <br>
    <label for="nation">Nationality:</label>
    <br>
    <br>
    <select name="nation" id="nation">
        <option value="indonesian">Indonesian</option>
        <option value="singaporean">Singaporean</option>
        <option value="malaysian">Malaysian</option>
        <option value="australian">Australian</option>
      </select>
    <br>
    <br>
    <label for="lang">Language Spoken:</label>
    <br>
    <br>
    <input type="checkbox" id="lang1" name="lang1" value="indonesia">
    <label for="lang1"> Bahasa Indonesia</label><br>
    <input type="checkbox" id="lang2" name="lang2" value="english">
    <label for="lang2"> English</label><br>
    <input type="checkbox" id="lang3" name="lang3" value="other">
    <label for="lang3"> Other</label>
    <br>
    <br>
    <label for="bio">Bio:</label>
    <br>
    <br>
    <textarea id="w3review" name="w3review" rows="8" cols="25">
        </textarea>
    <br>
    <input type="submit" value="Sign Up">

</form>